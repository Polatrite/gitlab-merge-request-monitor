
(async function() {
	let projectId = YOUR_PROJECT_ID_HERE
	let accessToken = YOUR_ACCESS_TOKEN_HERE
	let gitlabUrl = 'https://gitlab.com/api/v4'

	let appState = {
		mergeRequests: {},
		mergeRequestCount: 0
	}

	async function ajaxRequest(options) {
		let { 
			method, 
			url,
			data = {}
		} = options

		const results = await $.ajax({
			url: url,
			data: { },
			type: 'GET',
			headers: {
				'PRIVATE-TOKEN': accessToken
			}
		})

		return results
	}

	async function getMergeRequests() {
		let mergeRequests = await ajaxRequest({
			method: 'GET', 
			url: `${gitlabUrl}/projects/${projectId}/merge_requests`
		})

		// get notes & approvals and add them to our mergeRequests
		await populateMergeRequestNotes(mergeRequests)
		await populateMergeRequestApprovals(mergeRequests)

		return mergeRequests
	}

	async function populateMergeRequestNotes(mergeRequests) {
		let notes = await Promise.all(mergeRequests.map(async (mr) => {
			mr.notes = await ajaxRequest({
				method: 'GET',
				url: `${gitlabUrl}/projects/${projectId}/merge_requests/${mr.iid}/notes`
			})
			return mr.notes
		}))

		return notes
	}

	async function populateMergeRequestApprovals(mergeRequests) {
		let approvals = await Promise.all(mergeRequests.map(async (mr) => {
			mr.approvals = await ajaxRequest({
				method: 'GET',
				url: `${gitlabUrl}/projects/${projectId}/merge_requests/${mr.iid}/approvals`
			})
			return mr.approvals
		}))

		return approvals
	}

	function mapMergeRequests(data) {
		let mergeRequests = data.map((mr) => {
			return {
				internalId: mr.iid,
				dateOpened: `${timeSince(new Date(mr.created_at)).join(', ')} ago`,
				externalIdentifier: parseExternalIdentifier(mr.title) || "<ID???>",
				approvalCountNeeded: (mr.approvals_before_merge || 0),
				approvalCount: (mr.approvals.approvers ? mr.approvals.approvers.length : -1),
				approvals: mr.approvals,
				noteCount: (mr.notes ? mr.notes.length : 0),
				webUrl: mr.web_url,
				notes: mr.notes.map((note, index) => { return {
					internalId: note.id,
					noteId: `[#${index + 1}]`,
					authorName: note.author.name,
					authorImage: note.author.avatar_url,
					resolvable: note.resolvable,
					resolved: note.resolved,
					system: note.system,
					type: note.type
				}}).filter((note) => {
					return note.resolvable === true
						&& note.resolved === false
						&& note.system === false
						&& note.type === "DiffNote"
				}),
				authorName: mr.author.name,
				authorImage: mr.author.avatar_url,
				__original: mr,
			}
		})
		return mergeRequests
	}

	async function updateDOM(state) {
		let bodySubstitutionTable = {
			"mergeRequestCount": "merge-request-count"
		}
		let elementSubstitutionTable = {
			"internalId": "internal-id",
			"dateOpened": "date-opened",
			"externalIdentifier": "external-identifier",
			"approvalCountNeeded": "approval-count-needed",
			"approvalCount": "approval-count",
			"approvals": "approvals",
			"noteCount": "note-count",
			"notes": "notes",
			"webUrl": "web-url",
		}
		let noteSubstitutionTable = {
			"noteId": "note-id",
			"authorImage": "note-author-image",
			"authorName": "note-author-name",
		}

		let bodyElement = $('body')

		bodyElement = performSubstitutions(bodyElement, state, bodySubstitutionTable)

		$('.merge-request-data').remove()
		let mrTemplate = $('#merge-request-template')
		Object.keys(state.mergeRequests).forEach((key, index) => {
			let mr = state.mergeRequests[key]

			// find the existing merge-request, or make a new one
			let mrElement = $(`#merge-request-id-${mr["internalId"]}`)
			if(!mrElement.length) {
				mrElement = mrTemplate.clone()
			} else {
				console.log('found existing merge request')
			}
			mrElement = performSubstitutions(mrElement, mr, elementSubstitutionTable)

			let noteTemplate = mrElement.find('#note-authors-template')
			mr.notes.forEach((note) => {
				// find the existing note, or make a new one
				let noteElement = mrElement.find(`#note-id-${note["internalId"]}`)
				if(!noteElement.length) {
					noteElement = noteTemplate.clone()
				} else {
					console.log('found existing note')
				}
				noteElement = performSubstitutions(noteElement, note, noteSubstitutionTable)
				noteElement.attr('id', `note-id-${note["internalId"]}`)
				noteElement.insertAfter(mrElement.find('#note-authors-template'))
				noteElement.removeClass('template')
			})

			mrElement.attr('id', `merge-request-id-${mr["internalId"]}`)
			mrElement.addClass('merge-request-data')
			mrElement.insertAfter('#merge-request-template')
			mrElement.removeClass('template')
		})
	}

	// perform variable substitutions in the appropriate IDs
	// handles IMG and A tags specially, otherwise replaces inner HTML
	function performSubstitutions(parentElement, dataSource, substitutionTable) {
		for(let token in substitutionTable) {
			let domId = substitutionTable[token]
			let element = parentElement.find(`#${domId}`).first()
			if(element.prop('tagName') === "IMG") {
				element.attr('src', dataSource[token])
			} else if(element.prop('tagName') === "A") {
				element.attr('href', dataSource[token])
			} else {
				element.html(dataSource[token])
			}
		}

		return parentElement
	}

	function parseExternalIdentifier(str) {
		return str.substring(0, str.indexOf(" "))
	}

	function updateMergeRequestState(existingMrs, newMrs) {
		let deletedMrs = []
		Object.keys(existingMrs).forEach((key, index) => {
			existingMrs[key].$dirty = false
		})

		newMrs.forEach((mr) => {
			existingMrs[mr.internalId] = mr
			existingMrs[mr.internalId].$dirty = true
		})

		Object.keys(existingMrs).forEach((key, index) => {
			if(!existingMrs[key].$dirty) {
				deletedMrs.push(existingMrs[key]) c
				existingMrs[key] = null
				delete existingMrs[key]
			}
		})

		return { currentMergeRequests: existingMrs, deletedMergeRequests: deletedMrs }
	}

	function timeSince(date, precision = 2) {
		let depth = 0
		let seconds = Math.floor((new Date() - date) / 1000)
		let timeSince = []

		let interval = Math.floor(seconds / 31536000)
		if (interval > 1) {
			timeSince.push(interval + " years")
			seconds -= interval * 31536000
			depth++
			if(depth == precision) return timeSince
		}
		interval = Math.floor(seconds / 2592000)
		if (interval > 1) {
			timeSince.push(interval + " months")
			seconds -= interval * 2592000
			depth++
			if(depth == precision) return timeSince
		}
		interval = Math.floor(seconds / 86400)
		if (interval > 1) {
			timeSince.push(interval + " days")
			seconds -= interval * 86400
			depth++
			if(depth == precision) return timeSince
		}
		interval = Math.floor(seconds / 3600)
		if (interval > 1) {
			timeSince.push(interval + " hours")
			seconds -= interval * 3600
			depth++
			if(depth == precision) return timeSince
		}
		interval = Math.floor(seconds / 60)
		if (interval > 1) {
			timeSince.push(interval + " minutes")
			seconds -= interval * 60
			depth++
			if(depth == precision) return timeSince
		}
		return [Math.floor(seconds) + " seconds"]
	}

	async function sleep(ms) {
		return new Promise((resolve) => {
			setTimeout(resolve, ms)
		})
	}

	async function main(state) {
		let counter = 1
		while(true) {
			let mergeRequests = await getMergeRequests()

			if(counter > 2) {
				mergeRequests.pop()
			}
			console.log('gitlab MRs', mergeRequests)

			mergeRequests = mapMergeRequests(mergeRequests)
			let {
				currentMergeRequests,
				deletedMergeRequests
			} = updateMergeRequestState(state.mergeRequests, mergeRequests)

			console.log('current', currentMergeRequests)
			console.log('deleted', deletedMergeRequests)
			state.mergeRequestCount = Object.keys(currentMergeRequests).length

			console.log('processed MRs', mergeRequests)

			await updateDOM(state)
		
			await sleep(3000)
			counter++
		}

	}


	await main(appState)


})()


