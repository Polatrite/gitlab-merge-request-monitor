# gitlab-merge-request-monitor

An easy-to-read single page tool to see the merge requests under a given project. Shows the number of approvals needed, and any outstanding comments that need to be addressed. Built as a standalone client-side app using jQuery, just run index.html.

## Usage instructions

1. Create an API Access Key to use with this application
1. Add your API Access Key as the "accessToken" in monitor.js
1. Specify a project to monitor
1. Open index.html
